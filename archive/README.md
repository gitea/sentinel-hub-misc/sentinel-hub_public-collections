# Public Collections

A repository of publicly available collections that are available for access through [Sentinel Hub](https://www.sentinel-hub.com/). Note that collections in this registry are available via Sentinel Hub, but owned and maintained by different providers.

## What is it for?

When data is shared on Sentinel Hub, anyone can analyze it and build services on top of it using a broad range of compute and data analytics products. Sharing data in the cloud lets data users spend more time on data analysis rather than data acquisition. This repository exists to help people promote and discover datasets that are available via Sentinel Hub resources.

## Available Collections

- [Sentinel-2 L2A 120m Mosaic](collections/sentinel-s2-l2a-mosaic-120)
- [CORINE Land Cover](collections/corine-land-cover)
- [Global Land Cover](collections/global-land-cover)
- [Water Bodies](collections/water-bodies)


## How are collections added to the registry?

Each dataset in this repository is described with the following information:

```
Name
Short description
Sentinel Hub end-point
CollectionID
Resolution
Temporal availability
Update frequency
Band information
Provider
More information
License
```

## How to make use of these collection

Check this [FAQ entry](https://www.sentinel-hub.com/faq/#how-to-visualize-own-collection-eobrowser) to see how to configure your Sentinel Hub configuration and/or visualise them in EO Browser.
